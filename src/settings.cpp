#include "defines.h"
#include "constants.h"
#include "settings.h"
#include "EEPROM.h"


float InitialOpening = 2.1;       // Initial opening of barn doors at home position
                                  // (distance between the two pivot points in cm)
float MaximumOpening = 15.5;      // Maximum distance to allow barn doors to open (30 deg == 2 hours)
//long PlanAheadTrueTimeMs = DEFAULT_PLANAHEAD_TIME;  // Real time in ms between plans
long PlanAheadSysTimeTicks = PLANAHEAD_TIME_MS * TICKS_PER_MS;   // System time in ticks between plans

/*

Settings layout in EEPROM
Byte(s) | Content
========+=================================
0-1     | Settings schema version
2-3     | Reserved
4-7     | InitialOpening (float)
8-11    | MaximumOpening (float)
16-19   | PlanAheadSysTimeTicks (int32)
*/

void readSettings_v1() {
    EEPROM.get(4, InitialOpening);
    EEPROM.get(8, MaximumOpening);
    EEPROM.get(12, PlanAheadSysTimeTicks);
}

void readSettings() {
    int version = -1;
    
    if(EEPROM.get(0, version) == 0xffff) return; // no settings exist
    switch(version) {
        case 1:
            readSettings_v1();
            break;
        default:
            // don't know how to read this version
            return;
    }
}

void saveSettings() {
    // Save setting
    EEPROM.put(4, InitialOpening);
    EEPROM.put(8, MaximumOpening);
    EEPROM.put(12, PlanAheadSysTimeTicks);
    // save version
    EEPROM.put(0, (int)SETTINGS_VERSION);
}
