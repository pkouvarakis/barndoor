#ifndef DEFINES_H
#define DEFINES_H

// Constants to set based on hardware construction specs
// Barn door geometry
#define STEP_SIZE_DEG       1.8     // Degrees rotation per step
#define MICRO_STEPS         32      // Number of microsteps per step
#define THREADS_PER_CM      8       // Number of threads in rod per cm of length
#define BASE_LEN_UPPER_CM   28.1    // Length from hinge to center of rod in cm
#define BASE_LEN_LOWER_CM   28      // Length from hinge to center of rod in cm

// Motor specs
#define MOTOR_ACCELERATION  1500L       // steps per sec^2 - need to test this
#define MOTOR_MAX_SPEED     750L       // steps per sec
//#define MOTOR_SLOW_SPEED        0.04    // cm/sec - no longer needed
#define MOTOR_VERY_SLOW_SPEED   0.01    // cm/sec

// Constants to set based on electronic construction specs
// Note that step pin MUST be connected to pin 9 since
// this is the output of timer 1 comparator (OC1A)
#ifdef NANO

#define PIN_OUT_STEP        9   // Arduino digital pin connected to DRV8825 step
#define PIN_OUT_DIRECTION   8   // Arduino digital pin connected to DRV8825 direction
#define PIN_OUT_ENABLE      10   // Arduino digital pin connected to DRV8825 enable

#define PIN_IN_START        5
#define PIN_IN_STOP         4
#define PIN_IN_REWIND       3
#define PIN_IN_DIRECTION    2

#define PIN_IN_START_LIMIT  A2
#define PIN_IN_END_LIMIT    A3

#else

#define PIN_OUT_STEP        9   // Arduino digital pin connected to DRV8825 step
#define PIN_OUT_DIRECTION   5   // Arduino digital pin connected to DRV8825 direction
#define PIN_OUT_ENABLE      6   // Arduino digital pin connected to DRV8825 enable

#define PIN_IN_START        A0
#define PIN_IN_STOP         A1
#define PIN_IN_REWIND       A2
#define PIN_IN_DIRECTION    A3

#ifdef PRO_MINI
#define PIN_IN_START_LIMIT  A6
#define PIN_IN_END_LIMIT    A7
#else
#define PIN_IN_START_LIMIT  A4
#define PIN_IN_END_LIMIT    A5
#endif

#endif

// Timing
#define PLANAHEAD_TIME_MS   15000L

#endif
