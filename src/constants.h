#ifndef _CONSTANTS_H_
#define _CONSTANTS_H_

#define TICKS_PER_MS        (F_CPU / 1000L)
#define TICKS_PER_US        (F_CPU / 1000000L)

#endif
