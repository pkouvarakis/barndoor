#include <Fsm.h>
#include "usb_control.h"
#include "defines.h"
#include "constants.h"
#include "events.h"
#include "settings.h"

extern Fsm barndoor;

typedef struct {
    char const *command;
    const int len;
    void (*action)(const char *params);
} Command;

// PROGMEM Helper
#define PGMSTR(x) (__FlashStringHelper*)(x)

// Responses
static const char RESP_OK[] PROGMEM = {">OK"};
static const char RESP_OK_PREFIX[] PROGMEM = {">OK: "};
static const char RESP_ERR_INV_PARAM[] PROGMEM = {">ERR: INVALID PARAMETER"};

// extenal references
long motor_position();
long usteps_to_time(long usteps);


//

void actionStart(const char *p) {
    barndoor.trigger(EVENT_START_BUTTON);
    Serial.println(PGMSTR(RESP_OK));
}

void actionStop(const char *p) {
    barndoor.trigger(EVENT_STOP_BUTTON);
    Serial.println(PGMSTR(RESP_OK));
}

void actionRewind(const char *p) {
    barndoor.trigger(EVENT_REWIND_BUTTON);
    Serial.println(PGMSTR(RESP_OK));
}

void actionAutoHome(const char *p) {
    barndoor.trigger(EVENT_AUTO_HOME);
    Serial.println(PGMSTR(RESP_OK));
}

void actionSetMinOpening(const char *p) {
    char *end;
    float newMinOpening = strtod(p, &end);
    if(end != p && *end == 0)
    {
        InitialOpening = newMinOpening;
        init_vars();
        Serial.println(PGMSTR(RESP_OK));
    } else {
        Serial.println(PGMSTR(RESP_ERR_INV_PARAM));
    }
}


void actionSetMaxOpening(const char *p) {
    char *end;
    float newMaxOpening = strtod(p, &end);
    if(end != p && *end == 0) {
        MaximumOpening = newMaxOpening;
        init_vars();
        Serial.println(PGMSTR(RESP_OK));
    } else {
        Serial.println(PGMSTR(RESP_ERR_INV_PARAM));
    }
}

void actionGetTime(const char *p) {
    Serial.print(PGMSTR(RESP_OK_PREFIX)); // ">OK: "
    Serial.println(micros());
}

void actionSetPlanTime(const char *p) {
    char *end;
    int32_t ticks = strtol(p, &end, 10);
    if(end != p && *end == 0) {
        PlanAheadSysTimeTicks = ticks;
        Serial.println(PGMSTR(RESP_OK));
    } else {
        Serial.println(PGMSTR(RESP_ERR_INV_PARAM));
    }
}

void actionGetInfo(const char *p) {
    Serial.print(F(">OK: Ver=2,PAT="));
    Serial.print(PLANAHEAD_TIME_MS);
    Serial.print(F(",TPMS="));
    Serial.print(TICKS_PER_MS);
    Serial.print(F(",MNO="));
    Serial.print(InitialOpening);
    Serial.print(F(",MXO="));
    Serial.println(MaximumOpening);
}

void actionGetStatus(const char *p) {
    long pos = motor_position();
    Serial.print(F(">OK: SPOS="));
    Serial.print(pos);
    Serial.print(F(",TPOS="));
    Serial.println(usteps_to_time(pos));
}

void actionSaveConfig(const char *p) {
    saveSettings();
    Serial.println(PGMSTR(RESP_OK));
}


#define DEF_COMMAND(cmd)         static const char COMMAND_##cmd[] PROGMEM = {#cmd};
#define COMMAND(cmd, action)     {COMMAND_##cmd, sizeof(#cmd)-1, action}
#define NULL_COMMAND             {NULL, 0, NULL}

DEF_COMMAND(GET_TIME);
DEF_COMMAND(START);
DEF_COMMAND(STOP);
DEF_COMMAND(REWIND);
DEF_COMMAND(AUTO_HOME);
DEF_COMMAND(SET_MIN_OPENING);
DEF_COMMAND(SET_MAX_OPENING);
DEF_COMMAND(SET_PLAN_TIME);
DEF_COMMAND(GET_INFO);
DEF_COMMAND(SAVE_CONFIG);
DEF_COMMAND(GET_STATUS);

// Commands are scanned using linear search in the following array
// Place more common or more time sensitive commands first
const Command Commands[] = {
    COMMAND(GET_TIME, actionGetTime), // define this first to minimize delays, since this command is time sensitive
    COMMAND(START, actionStart),
    COMMAND(STOP, actionStop),
    COMMAND(REWIND, actionRewind),
    COMMAND(AUTO_HOME, actionAutoHome),
    COMMAND(SET_MIN_OPENING, actionSetMinOpening),
    COMMAND(SET_MAX_OPENING, actionSetMaxOpening),
    COMMAND(SET_PLAN_TIME, actionSetPlanTime),
    COMMAND(GET_INFO, actionGetInfo),
    COMMAND(SAVE_CONFIG, actionSaveConfig),
    COMMAND(GET_STATUS, actionGetStatus),
    NULL_COMMAND
};


void UsbControl::loop(long timeleft) {
    //if (timeleft <= 0) return;

    int count = Serial.available();
    if(count) {
        if(count > COMMAND_BUFFER_SIZE - bufferLen)
        {
            count = COMMAND_BUFFER_SIZE - bufferLen;
        }
        char *pIn = buffer + bufferLen;
        Serial.readBytes(pIn, count);
        bufferLen += count;
        char *pEnd = (char *)memchr(pIn, '\r', count);
        if(pEnd == NULL && bufferLen == COMMAND_BUFFER_SIZE) {
            // this essentialy is a buffer overflow
            // Assume the last character in buffer is a CR
            pEnd = &(buffer[COMMAND_BUFFER_SIZE - 1]);
        }
        if(pEnd) {
            *pEnd = 0;
            pIn += count;
            processLine();
            bufferLen = pIn - (++pEnd);
            if(bufferLen) memmove(buffer, pEnd, bufferLen);
        }
    }
}


void UsbControl::processLine() {
    const char *line = buffer + strspn(buffer, "\r\n");

    if(*line == 0) return;

    const Command *pCmd = Commands;
    while(pCmd->action) {
        int res = strncmp_P(line, pCmd->command, pCmd->len);
        char delim = line[pCmd->len];
        if(res == 0 && (delim == ' ' || delim =='\0')) {
            pCmd->action(line + pCmd->len + 1);
            return;
        }
        pCmd++;
    }
    Serial.println(F(">UNKNOWN COMMAND"));
}